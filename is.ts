'use strict';

export const isObject = val => {
	return val != null && typeof val === 'object' && Array.isArray(val) === false;
};

export const isBoolean = val => {
	return val === true || val === false;
};
