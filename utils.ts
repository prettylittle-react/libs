export const sleep = (ms, data = {}, status = true) => {
	return new Promise((resolve, reject) => {
		console.warn('Warning, using sleep() in your application');

		return setTimeout(() => {
			if (status === false) {
				return reject(data);
			}

			return resolve(data);
		}, ms);
	});
};
