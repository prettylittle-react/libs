'use strict';

export const serialize = (obj, prefix = null) => {
	let str = [],
		p;

	for (p in obj) {
		if (obj.hasOwnProperty(p) && obj[p]) {
			let k = prefix ? prefix + '[' + p + ']' : p,
				v = obj[p];
			str.push(
				v !== null && typeof v === 'object'
					? serialize(v, k)
					: encodeURIComponent(k) + '=' + encodeURIComponent(v)
			);
		}
	}

	return str.join('&');
};

export const deserialize = str => {
	const hashes = str.slice(str.indexOf('?') + 1).split('&');
	return hashes.reduce((acc, hash) => {
		const [key, val] = hash.split('=');

		let foo = decodeURIComponent(key);

		const m = foo.match(/([^\[]+)\[([^\]]+)\]/);

		let value = decodeURIComponent(val);

		value = Number(value) || value;

		if (m) {
			if (!acc[m[1]]) {
				acc[m[1]] = {};
			}

			let xx = Number(m[2]) || m[2];

			acc[m[1]][xx] = value;

			return {...acc};
		}

		return {
			...acc,
			[foo]: value
		};
	}, {});
};
