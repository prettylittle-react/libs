'use strict';

export const encode = str => {
	let encoded = '';

	for (let i = 0; i < str.length; i++) {
		encoded += `&#${str.charCodeAt(i)};`;
	}

	return encoded;
};

export const slugify = str => {
	return str
		.toString()
		.toLowerCase()
		.trim()
		.replace(/&/g, '-and-')
		.replace(/[\s\W-]+/g, '-');
};

export const randomId = () => {
	return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
};

export const uniqueId = (prefix = '') => {
	return (
		prefix +
		randomId() +
		randomId() +
		'-' +
		randomId() +
		'-' +
		randomId() +
		'-' +
		randomId() +
		'-' +
		randomId() +
		randomId() +
		randomId()
	);
};
